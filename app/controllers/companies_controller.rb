class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :update, :destroy]

  def index
    @companies = Company.all.includes(:share_prices)
    json_response(@companies)
  end

  def create
    @company = Company.create!(company_params)
    json_response(@company, :created)
  end

  def show
    json_response(@company)
  end

  def update
    @company.update(company_params)
    head :no_content
  end

  def destroy
    @company.destroy
    head :no_content
  end

  private

  def company_params
    params.permit(:name, :ric, :description, :country)
  end

  def set_company
    @company = Company.find(params[:id])
  end
end

class SharePricesController < ApplicationController
  before_action :set_company

  def create
    @share_price = @company.share_prices.create!(share_price_params)
    json_response(@share_price, :created)
  end

  def hour
    @share_prices = @company.share_prices.where('created_at > ?', 1.hour.ago)
    json_response(@share_prices)
  end

  def day
    @share_prices = @company.share_prices.where('created_at > ?', 24.hours.ago)
    json_response(@share_prices)
  end

  def week
    @share_prices = @company.share_prices.where('created_at > ?', 1.week.ago)
    json_response(@share_prices)
  end

  private

  def share_price_params
    params.permit(:price)
  end

  def set_company
    @company = Company.includes(:share_prices).find(params[:company_id])
  end
end

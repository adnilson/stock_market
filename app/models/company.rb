class Company < ApplicationRecord
  has_many :share_prices, dependent: :destroy

  validates :name, :ric, :description, :country, presence: true
  validates :ric, uniqueness: true
end

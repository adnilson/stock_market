class SharePrice < ApplicationRecord
  belongs_to :company

  validates_presence_of :price
end

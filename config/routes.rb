Rails.application.routes.draw do
  resources :companies do
    resources :share_prices, only: :create

    get '/share_prices/hour', to: 'share_prices#hour'
    get '/share_prices/day', to: 'share_prices#day'
    get '/share_prices/week', to: 'share_prices#week'
  end
end

class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :ric, unique: true
      t.text :description
      t.string :country

      t.timestamps
    end
  end
end

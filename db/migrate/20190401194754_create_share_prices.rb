class CreateSharePrices < ActiveRecord::Migration[5.2]
  def change
    create_table :share_prices do |t|
      t.decimal :price
      t.datetime :created_at
      t.datetime :updated_at
      t.references :company, foreign_key: true
    end
    add_index :share_prices, :created_at
  end
end

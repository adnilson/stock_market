class AddPayloadIdToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :payload_id, :bigint
    add_index :companies, :payload_id
  end
end

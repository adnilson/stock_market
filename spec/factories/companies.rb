FactoryBot.define do
  factory :company do
    name { Faker::Company.name }
    ric { Faker::Alphanumeric.alpha 10 }
    description { Faker::Company.bs }
    country { Faker::Address.country }
  end
end

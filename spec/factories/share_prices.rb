FactoryBot.define do
  factory :share_price do
    price { Faker::Number.decimal(3, 3) }
    created_at { Faker::Date.between(1.week.ago, Date.today) }
    company { nil }
  end
end

require 'rails_helper'

RSpec.describe Company, type: :model do
  it { should have_many(:share_prices).dependent(:destroy) }

  it { should validate_uniqueness_of(:ric) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:ric) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:country) }
end

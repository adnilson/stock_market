require 'rails_helper'

RSpec.describe SharePrice, type: :model do
  it { should belong_to(:company) }

  it { should validate_presence_of(:price) }
end

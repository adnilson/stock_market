require 'rails_helper'

RSpec.describe 'SharePrices API' do
  let!(:company) { create(:company) }
  let(:company_id) { company.id }

  describe 'POST /companies/:company_id/share_prices' do
    let(:valid_attribute) { { price: '102,44' } }

    context 'when request attribute is valid' do
      before { post "/companies/#{company_id}/share_prices", params: valid_attribute }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/companies/#{company_id}/share_prices", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Price can't be blank/)
      end
    end
  end

  describe 'GET /companies/:company_id/share_prices/hour' do
    let!(:out_of_bounds_share_price) do
      create(:share_price, company_id: company_id, created_at: 1.hour.ago - 1.second)
    end
    let!(:share_prices) do
      create_list(:share_price, 4, company_id: company_id, created_at: rand(1.hour.ago..Time.now))
    end

    before { get "/companies/#{company_id}/share_prices/hour" }

    it 'gets the shared_prices within the last hour' do
      expect(json.size).to eq(4)
    end
  end

  describe 'GET /companies/:company_id/share_prices/day' do
    let!(:out_of_bounds_share_price) do
      create(:share_price, company_id: company_id, created_at: 24.hours.ago - 1.second)
    end
    let!(:share_prices) do
      create_list(:share_price, 4, company_id: company_id, created_at: rand(24.hours.ago..Time.now))
    end

    before { get "/companies/#{company_id}/share_prices/day" }

    it 'gets the shared_prices within the last 24 hours' do
      expect(json.size).to eq(4)
    end
  end

  describe 'GET /companies/:company_id/share_prices/week' do
    let!(:out_of_bounds_share_price) do
      create(:share_price, company_id: company_id, created_at: 1.week.ago - 1.second)
    end
    let!(:share_prices) do
      create_list(:share_price, 4, company_id: company_id, created_at: rand(1.week.ago..Time.now))
    end

    before { get "/companies/#{company_id}/share_prices/week" }

    it 'gets the shared_prices within the last week' do
      expect(json.size).to eq(4)
    end
  end
end
